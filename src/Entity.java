import java.awt.Color;
/**
 * Superclass for any object that is placed on the field.
 * Equipped with basic location getting/setting and guarantees that its children will be able to act when the field is updated.
 * @author Basset Hound
 *
 */
public abstract class Entity {
	protected Location location; // the entity's location in the simulation grid
	
	public abstract void act(Field currentField, Field updatedField);
	
	/**
     * Return the entity's location.
     * @return The entity's location.
     */
	public Location getLocation() {
		return this.location;
	}
	
	/**
     * Set the entity's location.
     * @param row The vertical coordinate of the location.
     * @param col The horizontal coordinate of the location.
     */
	public void setLocation(int row, int col) {
		this.location = new Location(row, col);
	}
	
	/**
     * Set the entity's location.
     * @param location The entity's location.
     */
	public void setLocation(Location newLocation) {
		this.location = newLocation;
	}
}
