import java.util.Iterator;
/**
 * A single unmoving fruit entity that serves to keep rabbits alive.
 * 
 * @author Basset Hound
 */
public class Fruit extends Entity {
	private boolean isEaten = false; // similar to isDead in the Animal class - signifies whether or not to continue re-placing the fruit on the field.

	/**
	 * Called every field update. Checks to see if a rabbit is adjacent, and, if so, makes the rabbit eat and removes itself from the field.
	 */
	@Override
	public void act(Field currentField, Field updatedField) {
		if (! isEaten) {
			updatedField.place(this);
			Rabbit eatingRabbit = findEater(updatedField, this.getLocation());
			if (eatingRabbit != null) {
				eatingRabbit.eat();
				this.isEaten = true;
			}
			
		}
	}
	
	/**
     * Return the position of the first-found rabbit. Adapted from Fox 's findFood method.
     * @param field The field in which it must look.
     * @param location Where in the field it is located.
     * @return Where food was found, or null if it wasn't.
     */
    private Rabbit findEater(Field field, Location location)
    {
        Iterator<Location> adjacentLocations = field.adjacentLocations(location);
        while(adjacentLocations.hasNext()) {
            Location where = adjacentLocations.next();
            Entity entity = field.getEntityAt(where);
            if(entity instanceof Rabbit) {
                Rabbit rabbit = (Rabbit) entity;
                if(rabbit.isAlive()) { // so that we aren't feeding a dead rabbit
                    return rabbit;
                }
            }
        }
        return null;
    }
}
