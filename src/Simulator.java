import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Collections;
import java.awt.Color;

// Added the following lines
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * A simple predator-prey simulator, based on a field containing
 * rabbits and foxes.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 * @modified Chuck Cusack, September, 2007
 * 
 */
public class Simulator {
    // Constants representing configuration information for the simulation.
    // The default width for the grid.
    private static final int DEFAULT_WIDTH = 50;
    // The default depth of the grid.
    private static final int DEFAULT_DEPTH = 50;
    // The probability that a fox will be created in any given grid position.
    private static final double FOX_CREATION_PROBABILITY = 0.02;
    // The probability that a rabbit will be created in any given grid position.
    private static final double RABBIT_CREATION_PROBABILITY = 0.08;
    // the number of trees that will start in the world.
    private static final int TREE_COUNT = 2;

    // The current state of the field.
    private Field field;
    // A second field, used to build the next stage of the simulation.
    private Field updatedField;
    // The current step of the simulation.
    private int step;
    // A graphical view of the simulation.
    private SimulatorView view;

    // Added this variable for use by the thread.
    private int numberSteps;
    
    // Added the following GUI stuff
    // The main window to display the simulation (and your buttons, etc.).
    // must be package-accessible to be able to be altered inside of button callback methods
    JButton runOneButton, runHundredButton, runXButton, resetButton;
    JTextField xTextField;
    private JFrame mainFrame;
    
    private JMenuBar menuBar;
    private JMenu fileMenu, helpMenu;
    private JMenuItem exitMenuItem, aboutMenuItem;
    
    /**
     * Construct a simulation field with default size.
     */
    public Simulator()
    {
        this(DEFAULT_DEPTH, DEFAULT_WIDTH);
    }
    
    public static void main(String[] args) {
        // Create the simulator
        Simulator s = new Simulator();
    }
    /**
     * Create a simulation field with the given size.
     * @param depth Depth of the field. Must be greater than zero.
     * @param width Width of the field. Must be greater than zero.
     */
    public Simulator(int depth, int width)
    {
        if(width <= 0 || depth <= 0) {
            System.out.println("The dimensions must be greater than zero.");
            System.out.println("Using default values.");
            depth = DEFAULT_DEPTH;
            width = DEFAULT_WIDTH;
        }
        field = new Field(depth, width);
        updatedField = new Field(depth, width);

        // Create a view of the state of each location in the field.
        view = new SimulatorView(depth, width);
        
        view.setColor(Rabbit.class, Color.gray);
        view.setColor(Fox.class, Color.blue);
        view.setColor(Fruit.class, Color.pink);
        view.setColor(Tree.class, Color.green);
        
        // The rest of this method has changed.  This sets up the
        // JFrame to display everything.
        
        mainFrame = new JFrame();
        
        mainFrame.setTitle("Fox and Rabbit Simulation");
        
    	// Add window listener so it closes properly.
    	// when the "X" in the upper right corner is clicked.
    	mainFrame.addWindowListener( new WindowAdapter() {
                    public void windowClosing(WindowEvent e) {
                        System.exit(0);
                    }});  
        
        // Add a button to run 1 steps.
        runOneButton = new JButton("Run 1 step");
        runOneButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
        		simulateOneStep();
            }
        });
        
        // Add a button to run 100 steps.
        runHundredButton = new JButton("Run 100 steps");
        runHundredButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	simulate(100);
            }
        });
        
        // Add a button to run a customizable number of steps.
        runXButton = new JButton("Run X steps");
        JLabel xLabel = new JLabel("X:", JLabel.CENTER);
        xTextField = new JTextField(4);
        runXButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		try {
        			int x = Integer.parseInt(xTextField.getText());
        			simulate(x);
        		}
        		catch (NumberFormatException err) {
        			xTextField.setText("");
        			xTextField.setToolTipText("Enter a valid number.");
        		}
        	}
        });
        
        // Add a button to reset the field and repopulate it.
        resetButton = new JButton("Reset");
        resetButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
		        field = new Field(DEFAULT_DEPTH, DEFAULT_WIDTH);
		        updatedField = new Field(DEFAULT_DEPTH, DEFAULT_WIDTH);
		        reset();
			}
        	
        });
        
        // Adding things to a JFrame first requires that you get the
        // content pane.  Notice you don't do with with a JPanel.
        Container northContainer = new Container();
        northContainer.setLayout(new FlowLayout());
        northContainer.add(runOneButton);
        northContainer.add(runHundredButton);
        northContainer.add(runXButton);
        northContainer.add(xLabel);
        northContainer.add(xTextField);
        
//        Container southContainer = new Container();
//        southContainer.setLayout(new FlowLayout());
//        southContainer.add(resetButton);
        
        Container contents = mainFrame.getContentPane();
        contents.add(view, BorderLayout.CENTER);
        contents.add(northContainer, BorderLayout.NORTH);
        contents.add(resetButton, BorderLayout.SOUTH);
        
        // Set up the menu bar
        
        menuBar = new JMenuBar();
        
        fileMenu = new JMenu("File");
        exitMenuItem = new JMenuItem("Exit");
        exitMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
        });
        fileMenu.add(exitMenuItem);
        
        helpMenu = new JMenu("Help");
        aboutMenuItem = new JMenuItem("About");
        aboutMenuItem.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		JOptionPane.showMessageDialog(mainFrame,
        			    "This program simulates a field in which foxes interact with bunnies"
        			    + " and bunnies interact with the fruit dropped from trees.");
        	}
        });
        helpMenu.add(aboutMenuItem);
        
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        
        mainFrame.setJMenuBar(menuBar);
        
        // You always need to call pack on a JFrame.
        mainFrame.pack();
        
        // For very subtle reasons, this MUST go after the pack statement above.
        // Just trust me on this one.  Or figure out why for yourself.
        reset();
        
        // You always need to call setVisible(true) on a JFrame.
        mainFrame.setVisible(true);
    }
    
    /**
     * Run the simulation from its current state for a reasonably long period,
     * e.g. 500 steps.
     */
    public void runLongSimulation()
    {
        simulate(500);
    }
    
    /**
     * Run the simulation from its current state for the given number of steps.
     * Stop before the given number of steps if it ceases to be viable.
     * This has been modified so it uses a thread--this allows it to work
     * in conjunction with Swing.
     * Modified by Chuck Cusack, Sept 18, 2007
     * 
     * @param numSteps How many steps to run for.
     */    
    public void simulate(int numSteps)
    {
            // For technical reason, I had to add numberSteps as a class variable.
            numberSteps=numSteps;
            // Create a thread
            Thread runThread = new Thread() {
              // When the thread runs, it will simulate numberSteps steps.
              public void run() {
                   // Disable the button until the simulation is done.
                    runOneButton.setEnabled(false);
            	  	runHundredButton.setEnabled(false);
            	  	runXButton.setEnabled(false);
                    for(int step = 1; step <= numberSteps && view.isViable(field); step++) {
                        simulateOneStep();
                    }
                    // Now re-enable the button
                    runOneButton.setEnabled(true);
            	  	runHundredButton.setEnabled(true);
            	  	runXButton.setEnabled(true);
              }
            };
            // Start the thread
            runThread.start();
            // Now this method exits, allowing the GUI to update.  The simulation is being
            // run on a different thread, so the GUI updates as the simulation continues.
    }
    
    /**
     * Run the simulation from its current state for a single step.
     * Iterate over the whole field updating the state of each
     * fox and rabbit.
     */
    public void simulateOneStep()
    {
        step++;
        
        // let all animals act
        ArrayList<Entity> entities = field.getEntities();
        Collections.shuffle(entities); // to randomize the order they act.
        for(Iterator<Entity> it = entities.iterator(); it.hasNext(); ) {
            Entity entity = it.next();
            entity.act(field, updatedField);
        }
        
        // Swap the field and updatedField at the end of the step.
        Field temp = field;
        field = updatedField;
        updatedField = temp;
        updatedField.clear();

        // Display the new field on screen.
        view.showStatus(step, field);
    }
        
    /**
     * Reset the simulation to a starting position.
     */
    public void reset()
    {
        step = 0;
        field.clear();
        updatedField.clear();
        populate(field);
        
        // Show the starting state in the view.
        view.showStatus(step, field);
    }
    
    /**
     * Populate a field with foxes and rabbits.
     * @param field The field to be populated.
     */
    private void populate(Field field)
    {
        Random rand = new Random();
        field.clear();
        for(int row = 0; row < field.getDepth(); row++) {
            for(int col = 0; col < field.getWidth(); col++) {
                if(rand.nextDouble() <= FOX_CREATION_PROBABILITY) {
                    Fox fox = new Fox(true);
                    fox.setLocation(row, col);
                    field.place(fox);
                }
                else if(rand.nextDouble() <= RABBIT_CREATION_PROBABILITY) {
                    Rabbit rabbit = new Rabbit(true);
                    rabbit.setLocation(row, col);
                    field.place(rabbit);
                }
                // else leave the location empty.
            }
        }
        // adding trees
        for (int i = 0; i < TREE_COUNT; i++) {
        	int col = rand.nextInt(field.getWidth());
        	int row = rand.nextInt(field.getDepth());
        	Tree tree = new Tree();
        	tree.setLocation(row, col);
        	field.place(tree);
        }
    }
}
