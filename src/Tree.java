import java.util.Random;

/**
 * A single unmoving tree entity that spawns fruits around it per update.
 * @author Basset Hound
 *
 */
public class Tree extends Entity {
	public static final int REACH = 6; // the range, from the tree's origin, at which the tree can drop fruit.
	private final Random rand = new Random(); // a random number generator.
	
	/**
	 * Called every time the field is updated. Attempts to put 1 fruit into a nearby tile.
	 */
	@Override
	public void act(Field currentField, Field updatedField) {
		updatedField.place(this); // trees will never die
		Location newFruitPosition = getNewFruitPosition(updatedField);
		if (newFruitPosition != null) {
			Fruit newFruit = new Fruit();
			newFruit.setLocation(newFruitPosition);
			updatedField.place(newFruit);
		}
	}
	
	/**
	 * Get a random location within the Tree's reach + 1.
	 * @param updatedField - the field with which to get a free location.
	 * @return - if a free location was found, that location, otherwise null.
	 */
	public Location getNewFruitPosition(Field updatedField) {
		int newCol = this.getLocation().getCol() + rand.nextInt(REACH * 2 + 1) - REACH; // anywhere from x-6 to x+6
		int newRow = this.getLocation().getRow() + rand.nextInt(REACH * 2 + 1) - REACH; // anywhere from y-6 to y+6
		int w = updatedField.getWidth();
		int d = updatedField.getDepth();
		while ((newCol > d-1 || newCol < 0) || (newRow > w-1 || newRow < 0)) {
			newCol = this.getLocation().getCol() + rand.nextInt(REACH * 2 + 1) - REACH;
			newRow = this.getLocation().getRow() + rand.nextInt(REACH * 2 + 1) - REACH;
		}
		return updatedField.freeAdjacentLocation(new Location(newRow, newCol)); // we want to actually be able to place the fruit, so freeAdjacentLocation is used
	}
}
