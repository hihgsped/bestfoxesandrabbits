import java.awt.Color;
import java.util.Random;

/**
 * A simple model of a rabbit. Rabbits age, move, breed, and die.
 * 
 * @author David J. Barnes and Michael Kolling
 * @version 2006.03.30
 */
public class Rabbit extends Animal {
	// Characteristics shared by all rabbits (static fields).

	// The age at which a rabbit can start to breed.
	private static final int	BREEDING_AGE			= 5; // changed so that most rabbits will only breed if they eat fruit
	// The age to which a rabbit can live.
	private static final int	MAX_AGE					= 7; // changed so that rabbits that don't eat fruit will die
	// The likelihood of a rabbit breeding.
	private static final double	BREEDING_PROBABILITY	= 0.10; // tuned down slightly for no real reason
	// The maximum number of births.
	private static final int	MAX_LITTER_SIZE			= 3; // unchanged
	
	// The number to set the bunny's belly to when it eats a fruit.
	private static final int    MAX_BELLY_SIZE          = 5;
	// A shared random number generator to control breeding.
	private static final Random	rand					= new Random();

	// Individual characteristics (instance fields).
	// how many turns the rabbit is incapacitated because it is fat (lethargic, maybe?)
	private int belly;
	// how many more years the rabbit will live because it has been eating fruit because the fruit is magical life-extending fruit for some reason
	private int fruitScore;

	/**
	 * Create a new rabbit. A rabbit may be created with age zero (a new born) or with a random age.
	 * 
	 * @param randomAge If true, the rabbit will have a random age.
	 */
	public Rabbit(boolean randomAge) {
		super();
		this.belly = 0;
		this.fruitScore = 0;
		if (randomAge) {
			setAge(rand.nextInt(MAX_AGE));
		}
	}

	/**
	 * This is what the rabbit does most of the time - it runs around. Sometimes it will breed or die of old age.
	 * 
	 * @param currentField The field currently occupied.
	 * @param updatedField The field to transfer to.
	 */
	public void act(Field currentField, Field updatedField) {
		decrementBelly();
		incrementAge();
		if (isAlive()) {
			int births = breed();
			// Attempt to have births children
			for (int b = 0; b < births; b++) {
				// New code (Added January 28, 2013 by CAC)
				Location newOne = updatedField.freeAdjacentLocation(getLocation());
				if (newOne != null) {
					// Only create the new rabbit if there is somewhere to put it.
					Rabbit newRabbit = new Rabbit(false);
					newRabbit.setLocation(newOne);
					updatedField.place(newRabbit);
				}
				// Old code (Removed January 28, 2013 by CAC)
				// Rabbit newRabbit = new Rabbit(false);
				// newAnimals.add(newRabbit);
				// newFox.setLocation(updatedField.randomAdjacentLocation(getLocation()));
				// updatedField.place(newRabbit);
			}
			Location newLocation = updatedField.freeAdjacentLocation(getLocation());
			// Only transfer to the updated field if there was a free location
			if (this.isFat()) { // we want to stop the rabbit from moving for a couple of turns (specifically MAX_BELLY_SIZE turns)
				// don't change the location
				updatedField.place(this);
			}
			else if (newLocation != null) {
				setLocation(newLocation);
				updatedField.place(this);
			} else {
				// can neither move nor stay - overcrowding - all locations taken
				setDead();
				// conway's game of life???
			}
		}
	}
	
	/**
	 * Sets the bunny's belly to its maximum size. Accessed by the Fruit class to signify the rabbit has eaten it.
	 */
	public void eat() {
		this.belly = MAX_BELLY_SIZE;
		this.fruitScore += 7;
	}
	
	/**
	 * Decreases the number of turns the bunny is considered fat.
	 */
	private void decrementBelly() {
		if (this.belly > 0) {
			this.belly --;
		}
	}

	/**
	 * Tells whether the rabbit has recently eaten a fruit.
	 * @return - True if the rabbit has eaten a fruit in recent iterations of the simulation.
	 */
	private boolean isFat() {
		return this.belly > 0;
	}

	/**
	 * Increase the age. This could result in the rabbit's death.
	 */
	private void incrementAge() {
		setAge(getAge() + 1);
		if (getAge() > MAX_AGE + this.fruitScore) { // the rabbit's max_age is increased by the life fruit!
			setDead();
		}
	}

	/**
	 * Generate a number representing the number of births, if it can breed.
	 * 
	 * @return The number of births (may be zero).
	 */
	private int breed() {
		int births = 0;
		if (canBreed() && rand.nextDouble() <= BREEDING_PROBABILITY) {
			births = rand.nextInt(MAX_LITTER_SIZE) + 1;
		}
		return births;
	}

	/**
	 * @return A string representation of the rabbit.
	 */
	public String toString() {
		return "Rabbit, age " + getAge();
	}

	/**
	 * A rabbit can breed if it has reached the breeding age.
	 */
	private boolean canBreed() {
		return getAge() >= BREEDING_AGE;
	}
}
